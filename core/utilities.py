import matplotlib.pyplot as plt
import statsmodels.api as stm


def plot_poisson(vals, counts):
    """Plots model results.

    Args:
        vals (np.ndarray): ordered array of unique total of flash
            observations.
        counts (np.ndarray): frequency of `vals`.

    Returns:
        None
    """
    plt.stem(vals, counts)
    plt.xlabel("Flash count")
    plt.ylabel("Frequency")
    plt.title("Training distribution of counted flashes")
    plt.show()


def sm_poisson_check(shaped_data):
    """Uses statsmodels to fit the data for later comparison.

    Args:
        shaped_data (tuple): Training and testing data from
            data_parser.shape_data().

    Returns:
        poisson_results (GLMResultsWrapper): summary of statsmodels
            results.
    """
    poisson_family = stm.families.family.Poisson(link=stm.genmod.families.links.log)
    poisson_model = stm.GLM(shaped_data[-1], shaped_data[-2], family=poisson_family)
    poisson_results = poisson_model.fit()
    print(type(poisson_results))
    return poisson_results
