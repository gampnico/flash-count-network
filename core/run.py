import numpy as np
import pandas as pd
import torch

device = torch.device("cuda:0")
from sklearn.metrics import roc_curve, auc
import os
import sys
import matplotlib.pyplot as plt
import backend
import time

os.getcwd()

data = pd.read_csv("./data/simdata.csv")

# Group variables in list
variables = ["x1", "x2", "x3", "lon", "lat"]
location = ["lon", "lat"]

all_variables = variables  # A + B +... used if splitting variables into groups


variable_tensor = torch.tensor(data[variables].to_numpy(), dtype=torch.float)
location_tensor = torch.tensor(data[location].to_numpy(), dtype=torch.float)

all_tensors = [variable_tensor]  # Again in case of splitting variables into groups

y_tensor = torch.tensor(data["counts"].to_numpy(), dtype=torch.float)
output_tensor = torch.tensor(data["counts"].to_numpy(), dtype=torch.float)

# Split data
db_size = 20000
test_db_size = int(db_size * 0.2)
train_db = data[: db_size - test_db_size]
test_db = data[db_size - test_db_size : db_size]
train_variable_tensor = torch.tensor(train_db[variables].to_numpy(), dtype=torch.float)
train_all_tensors = [train_variable_tensor]
test_variable_tensor = torch.tensor(test_db[variables].to_numpy(), dtype=torch.float)
test_all_tensors = [test_variable_tensor]
y_tensor_train = torch.tensor(train_db["counts"].to_numpy(), dtype=torch.float)
output_tensor_train = torch.tensor(train_db["counts"].to_numpy(), dtype=torch.float)
y_tensor_test = torch.tensor(test_db["counts"].to_numpy(), dtype=torch.float)
output_tensor_test = torch.tensor(test_db["counts"].to_numpy(), dtype=torch.float)

# Debug in case of tensor shape mismatch
# print(data[variables].to_numpy().shape)
# print(data[variables].to_numpy()[0].shape)
# print(data["counts"].to_numpy().shape)
# print(data[variables].to_numpy())
# for x in all_tensors:
#     print(x.shape)
# print(y_tensor.shape)
# print(output_tensor.shape)

torch.manual_seed(444)

model = backend.Net(
    tensor_groups=[variables],
    group_names=["X Variables"],
)
optimiser = torch.optim.Adam(model.parameters(), lr=0.001)
print(model)
# y_pred = model(all_tensors)
loss_in_store = []
disp_grad_store = []

start = time.time()
# for t in range(3000):
#     # Forward pass
#     y_pred = model(train_all_tensors)
#     # Loss
#     loss_in = backend.nb2_loss(y_tensor_train, y_pred, model.dispersion)
#     loss_in_store.append(loss_in.item())
#     disp_grad_store.append(model.dispersion.grad)
#     optimiser.zero_grad()
#     loss_in.backward()
#     optimiser.step()
#     if t % 100 == 0:
#         loss_out = backend.nb2_loss(output_tensor_train, y_pred, model.dispersion)
#         print(
#             f"iter {t}: loss in = {loss_in.item():.5f}, loss out = {loss_out.item():.5f}"
#         )


for t in range(3000):
    # Forward pass
    y_pred = model(train_all_tensors)
    # Loss
    loss_in = backend.poisson_loss(y_tensor_train, y_pred)
    loss_in_store.append(loss_in.item())

    optimiser.zero_grad()
    loss_in.backward()
    optimiser.step()
    if t % 100 == 0:
        loss_out = backend.poisson_loss(output_tensor_train, y_pred)
        print(
            f"iter {t}: loss in = {loss_in.item():.5f}, loss out = {loss_out.item():.5f}"
        )
end = time.time()

# Metrics
print(model.coeff_table())
print(model.loading_table())


# Poisson comparison

pred_pd = pd.Series(y_pred.exp().detach().numpy())
disp_val = model.dispersion.exp().item()


nb_fit = backend.nb_fit(mu=pred_pd, obs=train_db["counts"], disp=disp_val, max_y=10)
print("\nNB Fitting\n")
print(nb_fit)

pois_fit = backend.nb_fit(mu=pred_pd, obs=train_db["counts"], disp=1, max_y=10)
print("\nPoisson Fitting\n")
print(pois_fit)

nb_fit_y2 = backend.nb_fit(mu=pred_pd, obs=data["counts"], disp=disp_val, max_y=10)

print(nb_fit_y2)

# # ROC/AUC

# data["Weights"] = data["counts"].clip(1)
# data["Outcome"] = data["counts"].clip(0, 1)

# fpr, tpr, thresh = roc_curve(data["Outcome"], pred_pd, sample_weight=data["Weights"])
# weighted_auc = auc(fpr, tpr)
# print(f"AUC: {weighted_auc:.6f}")
print(end - start)

plt.plot(loss_in_store)
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.show()