"""Nicolas Gampierakis

Data pre-processing.
"""
import numpy as np
from sklearn.model_selection import train_test_split


def import_simdata(filepath="./data/simdata.csv"):
    """Takes simulated data and loads it into a numpy array.

    Args:
        filepath (str): path to simulated data.

    Returns:
        df (np.ndarray): data array of input csv file.
    """
    df = np.loadtxt(filepath, skiprows=1, delimiter=",")
    print("SimData imported")
    return df


def shape_data(data):

    """Splits `data` into training and testing sets.

    Args:
        data (np.ndarray): array containing observations and independent
            variables.

    Returns:
        (tuple):
            - X_train (np.ndarray): Training data, independent
                variables, ordered (x1, x2, x3, lon, lat)
            - y_train (np.ndarray): Training data, flash count, ordered.
            - X_test (np.ndarray): Testing data, independent variables,
                ordered.
            - y_test (np.ndarray): Testing data, flash count, ordered.
            - X (np.ndarray): All independent variables, ordered.
            - y (np.ndarray): All dependent variables, ordered.
    :rtype(np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray,
        np.ndarray)
    """

    # Set seeds
    seed_number = 444
    np.random.seed(seed_number)

    # pretty print
    np.set_printoptions(precision=3)

    X = data[..., 1 : (np.size(data, axis=1) - 1)]
    y = data[..., 0]
    X = np.array(X, dtype="float32")
    y = np.array(y, dtype="float32")

    # Split data
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42, shuffle=True
    )
    print("Data reshaped")
    # print(np.max(y_train))
    # print(np.max(y_test))
    # print(X_test[y_test >= 5])  # x1, x2, x3, lon, lat
    # print(X_test[y_test == np.max(y_test)])
    return X_train, y_train, X_test, y_test, X, y
