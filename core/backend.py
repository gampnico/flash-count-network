import torch
import numpy as np
import pandas as pd
from scipy.special import gamma, factorial


class HiddenSoftMax(torch.nn.Module):
    def __init__(self, in_features, out_features):
        super(HiddenSoftMax, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = torch.nn.Parameter(
            torch.ones(in_features, out_features, requires_grad=True) - 0.5
        )

    def loadings(self):
        cl = self.weight.clamp(0, 1)
        return cl / cl.sum().clamp(0.000001)  # to prevent divide by zero

    def forward(self, input):
        return input.mm(self.loadings())

    def extra_repr(self):
        return f"in_features={self.in_features}, out_features={self.out_features}"


class Net(torch.nn.Module):
    def __init__(self, tensor_groups, group_names):
        super(Net, self).__init__()
        latent_coefficient = []
        for x in tensor_groups:
            latent_coefficient.append(HiddenSoftMax(len(x), 1))
        self.lat_coeffs = torch.nn.ModuleList(latent_coefficient)
        self.lat_names = group_names
        self.lat_coeff_names = tensor_groups
        self.final_coeff = torch.nn.Parameter(
            torch.ones(len(group_names), requires_grad=True) - 0.5
        )
        self.final_int = torch.nn.Parameter(torch.zeros(1, requires_grad=True))
        self.dispersion = torch.nn.Parameter(torch.zeros(1, requires_grad=True))

    def loading_table(self):
        returned_li = []
        for h, v, n in zip(self.lat_coeffs, self.lat_coeff_names, self.lat_names):
            load = list(h.loadings().detach().numpy()[:, 0])
            df = pd.DataFrame(zip(v, load), columns=["Variable", "Loading"])
            df["Name"] = n
            returned_li.append(df.copy())
        returned_df = pd.concat(returned_li, axis=0)
        return returned_df

    def coeff_table(self):
        var_names = ["Intercept"] + self.lat_names + ["Dispersion"]
        coeff1 = self.final_coeff.clamp(0).detach().numpy()
        coeff2 = self.final_int.detach().numpy()
        coeff3 = self.dispersion.detach().numpy()
        var_coeff = list(coeff2) + list(coeff1) + list(coeff3)
        dat = pd.DataFrame(
            zip(var_names, var_coeff), columns=["Name", "LinearCoefficient"]
        )
        dat["IRR"] = np.exp(dat["LinearCoefficient"])
        return dat

    def forward(self, x):
        lat = []
        for x, h in zip(x, self.lat_coeffs):
            lat.append(h(x))
        latent = torch.cat(lat, axis=1)
        linear_pred = (latent * self.final_coeff.clamp(0)).sum(dim=1) + self.final_int
        return linear_pred


def nb2_loss_old(y_true, y_pred, disp):
    """-NLL(z|µ,θ)= -NLL(y_true|ln(y_pred), disp)"""
    inv_psi = 1 / disp.exp()
    mu = y_pred.exp()
    p = 1 / (1 + disp.exp() * mu)
    nll = (
        torch.lgamma(inv_psi + y_true)
        - torch.lgamma(y_true + 1)
        - torch.lgamma(inv_psi)
    )
    nll += inv_psi * torch.log(p) + y_true * torch.log(1 - p)
    return -nll.mean()


def nb2_loss(y_true, y_pred, disp):
    """-NLL(z|µ,θ)= -NLL(y_true|ln(y_pred), disp)"""
    inv_psi = 1 / disp.exp()
    mu = y_pred.exp()
    p = 1 / (1 + disp.exp() * mu)
    nll = (
        torch.lgamma(inv_psi)
        + torch.lgamma(y_true + 1)
        - torch.lgamma(inv_psi + y_true)
        - inv_psi * torch.log(p)
        - y_true * torch.log(1 - p)
    )

    # returning nll.sum() makes no difference, and is slower.
    return nll.mean()


def poisson_loss(y_true, y_pred):
    loss = torch.mean(y_pred - y_true * torch.log(y_pred))
    return loss


def pred_nb(mu, disp, int_y):
    inv_disp = 1 / disp
    p1 = gamma(int_y + inv_disp) / (factorial(int_y) * gamma(inv_disp))
    p2 = (inv_disp / (inv_disp + mu)) ** inv_disp
    p3 = (mu / (inv_disp + mu)) ** int_y
    pfin = p1 * p2 * p3
    return pfin


def nb_fit(mu, obs, disp, max_y):
    res = []
    cum_fit = mu - mu
    for i in range(max_y + 1):
        pred_fit = pred_nb(mu=mu, disp=disp, int_y=i)
        pred_obs = obs == i
        res.append(
            (str(i), pred_obs.mean(), pred_fit.mean(), pred_obs.sum(), pred_fit.sum())
        )
        cum_fit += pred_fit
    fin_fit = 1 - cum_fit
    fin_obs = obs > max_y
    res.append(
        (
            str(max_y + 1) + "+",
            fin_obs.mean(),
            fin_fit.mean(),
            fin_obs.sum(),
            fin_fit.sum(),
        )
    )
    dat = pd.DataFrame(res, columns=["Count", "Obs", "Pred", "ObsN", "PredN"])
    return dat


def ten_int(a, b):
    rows = a.shape[0]
    cols = a.shape[1] * b.shape[1]
    return torch.einsum("ij,ik->ijk", a, b).reshape((rows, cols))
