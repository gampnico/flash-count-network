# Flash Count Network

## To do

- ~~Implement custom poisson loss function.~~
- ~~Implement negative binomial loss function.~~
- Implement zero-truncated negatvie binomial loss function.
- Add GPU support.
- Optimise layer settings.
- Implement AUC/ROC metrics.